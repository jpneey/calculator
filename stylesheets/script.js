
    console.log("created by: John Paul Burato");
    function thousands_separators(num) {
      var num_parts = num.toString().split(".");
      num_parts[0] = num_parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, "<span class='comma'>,</span>");
      return num_parts.join(".");
    }
    function slicer() {
      var sauce = document.getElementById('formula').innerHTML;
      var raw = sauce.replace(/<span class=\"operator\">/g, "");
      var sanitized = raw.replace(/<\/span>/g, "");
        
      var c = sanitized.slice(0,-1);
      document.getElementById('formula').innerHTML = c;
    }
    function cleared() {
      document.getElementById('answer').innerHTML = '<span class=\'operator\'>___</span>';
      document.getElementById('formula').innerHTML = '';
    }
    function calcu() {
      try {
        var sauce = document.getElementById('formula').innerHTML;
        var raw = sauce.replace(/<span class=\"operator\">/g, "");
        var sanitized = raw.replace(/<\/span>/g, "");
        var rounded = Math.round(eval(sanitized) * 100) / 100
        var str = '' + thousands_separators(rounded);
        if(str == 'undefined') {
          document.getElementById('answer').innerHTML = '___';
        }
        else {
          document.getElementById('answer').innerHTML = '<span class=\'operator\'>' + str + '</span>';
        }
      }
      catch(err) {
        document.getElementById('answer').innerHTML = '<span class="error">Syntax error :(</span>';
      }
    }
    function formula(e) {
      document.getElementById('formula').innerHTML += e;   
    }